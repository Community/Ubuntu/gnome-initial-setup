/* -*- mode: C; c-file-style: "gnu"; indent-tabs-mode: nil; -*- */
/*
 * Copyright (C) 2018 Canonical Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

/* Ubuntu changes page {{{1 */

#define PAGE_ID "ubuntu-changes"

#include "config.h"
#include "gis-ubuntu-changes-page.h"
#include "ubuntu-changes-resources.h"

#include <glib/gi18n.h>
#include <gio/gio.h>

struct _GisUbuntuChangesPagePrivate {
  GtkWidget *changes_image;
  GtkWidget *window_switcher_label;
  GtkWidget *launcher_label;
  GtkWidget *apps_button_label;
  GtkWidget *app_menu_label;
  GtkWidget *clock_calendar_label;
  GtkWidget *system_menu_label;
  GtkWidget *close_button_label;
};
typedef struct _GisUbuntuChangesPagePrivate GisUbuntuChangesPagePrivate;

G_DEFINE_TYPE_WITH_PRIVATE (GisUbuntuChangesPage, gis_ubuntu_changes_page, GIS_TYPE_PAGE);

static void
gis_ubuntu_changes_page_constructed (GObject *object)
{
  GisUbuntuChangesPage *page = GIS_UBUNTU_CHANGES_PAGE (object);
  GisUbuntuChangesPagePrivate *priv = gis_ubuntu_changes_page_get_instance_private (page);

  G_OBJECT_CLASS (gis_ubuntu_changes_page_parent_class)->constructed (object);

  gis_page_set_skippable (GIS_PAGE (page), TRUE);

  gis_page_set_complete (GIS_PAGE (page), TRUE);
  gtk_widget_show (GTK_WIDGET (page));
}

static void
allocate_image (GtkWidget *fixed,
		GdkRectangle *allocation,
		GisUbuntuChangesPage *page)
{
  GisUbuntuChangesPagePrivate *priv = gis_ubuntu_changes_page_get_instance_private (page);
  gint image_width, image_height;
  gint window_switcher_width, window_switcher_height;
  gint launcher_width, launcher_height;
  gint apps_button_width, apps_button_height;
  gint app_menu_width, app_menu_height;
  gint clock_calendar_width, clock_calendar_height;
  gint system_menu_width, system_menu_height;
  gint close_button_width, close_button_height;
  gint image_x = 0, image_y = 0;

  gtk_widget_get_preferred_width (priv->changes_image, NULL, &image_width);
  gtk_widget_get_preferred_height (priv->changes_image, NULL, &image_height);
  gtk_widget_get_preferred_width (priv->window_switcher_label, NULL, &window_switcher_width);
  gtk_widget_get_preferred_height (priv->window_switcher_label, NULL, &window_switcher_height);
  gtk_widget_get_preferred_width (priv->launcher_label, NULL, &launcher_width);
  gtk_widget_get_preferred_height (priv->launcher_label, NULL, &launcher_height);
  gtk_widget_get_preferred_width (priv->apps_button_label, NULL, &apps_button_width);
  gtk_widget_get_preferred_height (priv->apps_button_label, NULL, &apps_button_height);
  gtk_widget_get_preferred_width (priv->app_menu_label, NULL, &app_menu_width);
  gtk_widget_get_preferred_height (priv->app_menu_label, NULL, &app_menu_height);
  gtk_widget_get_preferred_width (priv->clock_calendar_label, NULL, &clock_calendar_width);
  gtk_widget_get_preferred_height (priv->clock_calendar_label, NULL, &clock_calendar_height);
  gtk_widget_get_preferred_width (priv->system_menu_label, NULL, &system_menu_width);
  gtk_widget_get_preferred_height (priv->system_menu_label, NULL, &system_menu_height);
  gtk_widget_get_preferred_width (priv->close_button_label, NULL, &close_button_width);
  gtk_widget_get_preferred_height (priv->close_button_label, NULL, &close_button_height);

  /* Place image horizontally so all labels fit */
  if (window_switcher_width > image_x)
    image_x = window_switcher_width;
  if (launcher_width > image_x)
    image_x = launcher_width;
  if (apps_button_width > image_x)
    image_x = apps_button_width;
  if (system_menu_width > image_x)
    image_x = system_menu_width;
  if (close_button_width > image_x)
    image_x = close_button_width;

  /* Place image vertically so all labels fit */
  if (app_menu_height > image_y)
    image_y = app_menu_height;
  if (clock_calendar_height > image_y)
    image_y = clock_calendar_height;

  /* Use remaining space to center image */
  image_x += (allocation->width - (image_width + image_x * 2)) / 2;
  image_y += (allocation->height - (image_height + image_y * 2)) / 2;

  gtk_fixed_move (GTK_FIXED (fixed), priv->changes_image, image_x, image_y);
  gtk_fixed_move (GTK_FIXED (fixed), priv->window_switcher_label, image_x - window_switcher_width, image_y + 46 - window_switcher_height / 2);
  gtk_fixed_move (GTK_FIXED (fixed), priv->launcher_label, image_x - launcher_width, image_y + 138 - launcher_height / 2);
  gtk_fixed_move (GTK_FIXED (fixed), priv->apps_button_label, image_x - apps_button_width, image_y + 278 - apps_button_height / 2);
  gtk_fixed_move (GTK_FIXED (fixed), priv->app_menu_label, image_x + 139 - app_menu_width / 2, image_y - app_menu_height);
  gtk_fixed_move (GTK_FIXED (fixed), priv->clock_calendar_label, image_x + 249, image_y - clock_calendar_height);
  gtk_fixed_move (GTK_FIXED (fixed), priv->system_menu_label, image_x + image_width, image_y + 46 - system_menu_height / 2);
  gtk_fixed_move (GTK_FIXED (fixed), priv->close_button_label, image_x + image_width, image_y + 125 - system_menu_height / 2);
}

static gboolean
activate_link (GtkLabel             *label,
               const gchar          *uri,
               GisUbuntuChangesPage *page)
{
  /* FIXME: Show changes */

  return TRUE;
}

static void
gis_ubuntu_changes_page_locale_changed (GisPage *page)
{
  gis_page_set_title (GIS_PAGE (page), _("What's new in Ubuntu"));
}

static void
gis_ubuntu_changes_page_class_init (GisUbuntuChangesPageClass *klass)
{
  GisPageClass *page_class = GIS_PAGE_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (klass), "/org/gnome/initial-setup/gis-ubuntu-changes-page.ui");
  gtk_widget_class_bind_template_child_private (GTK_WIDGET_CLASS (klass), GisUbuntuChangesPage, changes_image);
  gtk_widget_class_bind_template_child_private (GTK_WIDGET_CLASS (klass), GisUbuntuChangesPage, window_switcher_label);
  gtk_widget_class_bind_template_child_private (GTK_WIDGET_CLASS (klass), GisUbuntuChangesPage, launcher_label);
  gtk_widget_class_bind_template_child_private (GTK_WIDGET_CLASS (klass), GisUbuntuChangesPage, apps_button_label);
  gtk_widget_class_bind_template_child_private (GTK_WIDGET_CLASS (klass), GisUbuntuChangesPage, app_menu_label);
  gtk_widget_class_bind_template_child_private (GTK_WIDGET_CLASS (klass), GisUbuntuChangesPage, clock_calendar_label);
  gtk_widget_class_bind_template_child_private (GTK_WIDGET_CLASS (klass), GisUbuntuChangesPage, system_menu_label);
  gtk_widget_class_bind_template_child_private (GTK_WIDGET_CLASS (klass), GisUbuntuChangesPage, close_button_label);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (klass), allocate_image);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (klass), activate_link);

  page_class->page_id = PAGE_ID;
  page_class->locale_changed = gis_ubuntu_changes_page_locale_changed;
  object_class->constructed = gis_ubuntu_changes_page_constructed;
}

static void
gis_ubuntu_changes_page_init (GisUbuntuChangesPage *page)
{
  g_resources_register (ubuntu_changes_get_resource ());

  gtk_widget_init_template (GTK_WIDGET (page));
}

GisPage *
gis_prepare_ubuntu_changes_page (GisDriver *driver)
{
  return g_object_new (GIS_TYPE_UBUNTU_CHANGES_PAGE,
                       "driver", driver,
                       NULL);
}
